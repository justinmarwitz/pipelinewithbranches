# Intro
This repo mainly focusses on giving an example of using branches in Bitbucket in conjunction with Runners and variable replacement using pipelines.

## Playbooks
ssh - Takes an inventory file but runs against the local host. Grabs the SSH key of the host in the inventory, check if it's in the known_hosts file and if not, adds the ssh fingerprint. This is not the most secure method, but works fine for internal projects.

template_config - Takes the template message script that we've dropped the env specific message into and copies it to the host. Then runs the script so we can ensure it worked.

## Use
This may look complex, but it's really not too bad.

The biggest things to look out for here are going to be the variable names as there are a lot of different variables flying around. You also will need two branches and a runner setup as well!

Once you have all the repo vars, both branches, and a runner setup, you would simply enable pipelines and run the pipeline for a given branch you made a commit to. 

The pipeline run will figure out which branch it's being run against, set the message var in the template script accordingly, and then drop the message script on whatever host(s) Ansible is running against. Finally, run the script to echo the message.

## Notes
I want to note that I have not tested this code, so I can't guarantee it will work as-is. There may be a few tweaks needed, but that's part of the fun!

In my example, you could skip all the if/then stuff entirely. You could just define two steps, one that runs through the dev actions and another that runs through the prod actions (similar to what I do on line 50 of the pipelines file). Then you would simply call that step(s) in the pipeline definition for that environment.

So why am I doing it this way? Because I wanted to show how to handle some basic conditional logic as well as creation of variables and passing those variables into other steps.

Also of note, I created a step for the dev repo variable substitution but then for prod I'm running an extra step in the pipeline itself to handle the same function. Why? Again, simply to show that there are multuple ways to handle these things!

## Questions
If you have further questions, feel free to drop a [comment on my post](https://justinmarwitz.com/bitbucket-branches-variable-handling-in-runners) about this. I'll do my best to reply or point you in the right direction.